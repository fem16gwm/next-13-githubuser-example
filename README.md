## Hinweise zum Code

In diesem Beispiel Next.js/React Projekt werden zur Demonstration zwei verschiedene Ansätze verfolgt

1. Server Component route "/users"
   Der state (Daten mehrerer User) werden serverseitig gefetcht und das fertige statische html an den client geschickt
2. Client Component route "/user"
   Der state (Daten eines users) werden über eine api (api ordner) vom server gefetcht und client seitig gespeichert

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Or, run the the build script and start the compiled version of the app:

```bash
npm run build

npm run start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
