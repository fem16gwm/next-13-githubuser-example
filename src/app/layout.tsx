import Link from "next/link";
import "./globals.css";
import type { Metadata } from "next";

// const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
    title: "Github User App",
    description: "application for getting user information of github users",
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
    return (
        <html lang="en">
            <body className="flex flex-col items-center bg-slate-100">
                <nav className="flex justify-center items-center h-16 space-x-10 underline">
                    <Link href={"/user"}>search one user</Link>
                    <Link href={"/"}>home</Link>
                    <Link href={"/users"}>search many users</Link>
                </nav>
                <main className="flex flex-col items-center justify-center max-w-4xl w-full bg-white rounded-md p-4 sm:p-8 shadow-md">
                    <h1 className="mb-6 mt-2">Github User App</h1>
                    {children}
                </main>
            </body>
        </html>
    );
}
