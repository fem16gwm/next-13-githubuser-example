import { NextResponse } from "next/server";

//fetches a github user by username from the github api
export async function GET(request: Request) {
    const url = new URL(request.url);
    const params = new URLSearchParams(url.search);
    const username = params.get("username");

    const res = await fetch(`https://api.github.com/users/${username}`, {
        headers: {
            "Content-Type": "application/json",
            // "API-Key": "GITHUB_CLIENT_SECRET", // not needed yet, just in case we need to fetch from non public api endpoint
        },
    });
    const data = await res.json();

    if (data?.login) return NextResponse.json({ user: data }, { status: 200 });
    if (data?.message) return NextResponse.json({ message: data.message }, { status: 400 });
    return NextResponse.json({ message: "Server Error" }, { status: 500 });
}
