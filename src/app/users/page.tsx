import UserCardSmall from "../../components/UserCardSmall";
import { getManyGithubUsers } from "@/lib/github";

interface pagePropsType {}

export default async function page({}: pagePropsType) {
    const users: GitHubUser[] = await getManyGithubUsers();

    return (
        <div>
            {users.map((user) => (
                <UserCardSmall key={user.id} user={user} />
            ))}
        </div>
    );
}
