"use client";
import UserCard from "@/components/UserCard";
import UserInputForm from "@/components/UserInputForm";
import { useState } from "react";

export default function Home() {
    const [user, setUser] = useState<GitHubUser | null>(null);

    return (
        <>
            <UserInputForm setUser={setUser} />
            <div className="h-8"></div>
            <UserCard user={user} />
        </>
    );
}
