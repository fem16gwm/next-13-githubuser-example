import Link from "next/link";

export default function Home() {
    return (
        <>
            <h2>Server Component</h2>
            <Link className="underline" href="/user">
                Search one user
            </Link>

            <h2 className="mt-6">Client Component</h2>
            <Link className="underline" href="/users">
                Search many users
            </Link>
        </>
    );
}
