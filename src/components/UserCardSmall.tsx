interface UserCardSmallPropsType {
    user: GitHubUser;
}

export default function UserCardSmall({ user }: UserCardSmallPropsType) {
    return (
        <div className="w-full max-w-md flex flex-row p-3 m-2 border border-gray-200 rounded-md flex-1">
            <div className=" w-1/6 flex items-center">
                <img src={user.avatar_url} alt={"user-image-" + user.login} className="aspect-square rounded-full bg-gray-100" />
            </div>
            <div className="w-5/6 flex flex-col items-center justify-center">
                <h2>{user.login}</h2>
                <p>
                    visit on{" "}
                    <a className="underline" href={user.html_url}>
                        github
                    </a>
                </p>
            </div>
        </div>
    );
}
