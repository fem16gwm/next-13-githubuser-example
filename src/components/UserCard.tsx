interface UserCardPropsType {
    user: GitHubUser | null;
}

export default function UserCard({ user }: UserCardPropsType) {
    if (!user) return null;

    return (
        <div className="border border-gray-200 p-8 rounded-md max-w-lg w-full flex flex-col justify-center">
            <div className="flex justify-center border-b border-gray-200 pb-6 mb-4">
                <img src={user.avatar_url} alt="user-img" className="w-1/2 aspect-square rounded-full bg-gray-100" />
            </div>
            <div className="mb-6">
                <a href={user.html_url}>
                    <h2>{user.login}</h2>
                </a>
                <p className="mb-2">{user.location}</p>
                <p className="font-light">{user.bio ? user.bio : "no bio"}</p>
            </div>
            <table>
                <tbody>
                    <tr>
                        <td className="w-1/3">repos</td>
                        <td className="w-1/3">folower</td>
                        <td className="w-1/3">following</td>
                    </tr>
                    <tr>
                        <td className="w-1/3 font-light">{user.public_repos}</td>
                        <td className="w-1/3 font-light">{user.followers}</td>
                        <td className="w-1/3 font-light">{user.following}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}
