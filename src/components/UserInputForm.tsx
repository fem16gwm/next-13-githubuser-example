"use client";
import { useState } from "react";
import axios from "@/lib/axios";

interface UserInputFormProps {
    setUser: React.Dispatch<React.SetStateAction<GitHubUser | null>>;
}
export default function UserInputForm(props: UserInputFormProps) {
    const [userName, setUserName] = useState("");
    const [loading, setLoading] = useState(false);
    const [errMsg, setErrMsg] = useState("");

    async function fetchGithubUser() {
        setLoading(true);
        try {
            const response = await axios.get(`/github_user?username=${userName}`);
            props.setUser(response.data.user);
        } catch (error: any) {
            if (!error?.response) setErrMsg("No Server Response");
            else if (error?.response?.data?.message) setErrMsg(error?.response?.data?.message);
            else setErrMsg("An unexpected error occurred");
        }
        setLoading(false);
    }

    function onSubmitHandler(e: React.FormEvent<HTMLFormElement>) {
        e.preventDefault();
        setErrMsg("");
        fetchGithubUser();
    }

    function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
        e.preventDefault();
        setErrMsg("");
        setUserName(e.target.value);
    }

    return (
        <form
            onSubmit={onSubmitHandler}
            className="border border-gray-200 p-8 rounded-md max-w-lg w-full flex flex-col justify-center"
        >
            <label className="text-sm font-semibold text-slate-700 mb-1.5">github username:</label>
            <input
                className="bg-slate-100 border border-gray-200 rounded-md py-2 px-4"
                value={userName}
                onChange={onChangeHandler}
            />
            <p className="text-sm text-red-600 mb-2 h-4">{errMsg}</p>
            <button disabled={loading} className="bg-cyan-600 hover:bg-cyan-700 text-white" type="submit">
                Search User
            </button>
        </form>
    );
}
